1.4.0\_0.0.1\_pre\_alpha\_ExpCore版本远征辅助使用指南

- 修改config.json。
- 双击run.bat运行，右上角关闭。
- 需要上线前，关闭辅助。

------

Config.json修改指南

1. config.json需要修改的部分包括以下三个部分：Username、Password、GameServer
   
   Username部分写用户名、Password部分写密码、GameServer写地址。
   
   | 服务器名称 | 台服名称         | 服务器地址                        | 
   | ----- | ------------ | ---------------------------- | 
   | 胡德    | HMS Hood     | http://s2.hk.jianniang.com/  | 
   | 萨拉托加  | USS Saratoga | http://s3.hk.jianniang.com/  | 
   | 俾斯麦   | KMS Bismarck | http://s4.hk.jianniang.com/  | 
   | 声望    | HMS Renown   | http://s5.hk.jianniang.com/  | 
   | 纳尔逊   | HMS Nelson   | http://s6.hk.jianniang.com/  | 
   | 空想    | Le Fantasque | http://s7.hk.jianniang.com/  | 
   | 海伦娜   | USS Helena   | http://s8.hk.jianniang.com/  | 
   | 突击者   | USS Ranger   | http://s9.hk.jianniang.com/  | 
   | 黎塞留   | Richelieu    | http://s10.hk.jianniang.com/ | 
   | 贝尔法斯特 | HMS Belfast  | http://s11.hk.jianniang.com/ | 
   |       |              |                              | 
   
2. 正确的填写样本如下：
   
   ![Sample Image PlaceHolder](./sample.png)



