| 图序号  |   路径    |    练级目标     |         捞的对象         |    日常任务对象     |                  阵型                   | 
| :--: | :-----: | :---------: | :------------------: | :-----------: | :-----------------------------------: | 
| 3-4  |    A    |  战巡、战列、航战   |          狗粮          |   战巡、重巡、轻巡    | 主练+重炮+欧根+重炮+(重巡/轻巡/驱逐)+(重巡/轻巡/驱逐)－全复纵 | 
| 6-1  |    A    | 轻巡、重巡、驱逐、轻母 |         防空狗粮         |     雷巡、潜艇     |    2轻母＋3轻巡/驱逐＋1X(有1个低速)－反潜，SL掉敌航母     | 
| 3-2  |  C-E-G  |     6驱逐     | 狗粮、肇和、加贺、安德烈娅多利亚、天狼星 |      All      |              6驱逐(防空/复纵)               | 
| 5-3  |  B-G-H  |    航母+轻巡    |        赤城、大黄蜂        |      All      |                海伦娜+5航母                | 
| 3-1  | A-F-H-I |    战巡+驱逐    |        凉月、秋月         |      All      |                战巡+5驱逐                 | 
| 2-3  | A-D-G-J |     6驱逐     |          应瑞          |      All      |                  6驱逐                  | 
| 3-3  |         |             |        赤城、应瑞         |      All      |                                       | 
| 驱逐困难 |         |             |                      | 战列 * 1、驱逐 * 5 |                                       | 
| 巡洋困难 |         |             |                      |               |                                       | 
| 战列困难 |         |             |                      |               |                                       | 
| 航母困难 |         |             |                      |               |                                       | 

RoadMap

| Release     | 功能            | 注释                 | 
| ----------- | ------------- | ------------------ | 
| alpha-0.0.1 | 无尽远征          |                    | 
| alpha-0.0.2 | ＋自动修理，任务结束领取  | 国服/台服控制            | 
| alpha-0.1.0 | 自动换船＋远征、修理、演习 |                    | 
| alpha-0.1.1 | ＋刷3-4A        | 3-4A只有一个点，无SL，容易编写 | 
| alpha-0.1.2 | ＋刷6-1A        | 6-1A只有一个点，但是需要SL   | 
| alpha-0.1.3 | ＋自动刷战役        |                    | 
| beta-0.2.0  |               |                    | 
|             |               |                    | 
|             |               |                    | 

LoginServer/GameServer可以在登陆过程中获得



Definite Automatic Machine()



ChangeLog between alpha-0.0.1 and alpha 0.0.2

- [x] initial commit
- [x] 用args[]里的或者默认路径里的config.json来替代写死在代码中的用户设定。
- [x] 使用ScheduledExecutorService和scheduleWithFixedDelay而非Thread.sleep()来控制定时触发任务
- [x] 用com.alibaba.fastjson而非org.json包来简化JSON字符串解析过程，同时降低了代码和JSON结构的耦合
- [x] 用Java Beans类来控制用户信息管理而非不规范的全局变量
- [x] 处理{"eid”:-9996}而非单纯重新发出请求（wireshark抓包来比较请求包之间的不同，证明是MD5生成类的问题）。
- [ ] 在config.json中加入MaxInterval和MinInterval来确定两次请求之间的随机时间
- [x] 对于HttpClient包中的Exception，加入回退机制（重发RetryHandler）。
- [x] 写一个使用指南
- [ ] 选择服务器部分可选写服务器名、编号或者写域名，方便以后将AutomaticallyGetServer设定为true后的域名自动update功能
- [ ] 删除保存在git中的敏感信息
- [ ] 使用有限自动机在Utility层进行Cookies和登陆状态的管理{Platform Login、Unified ID Login、Cookies Session Refresh}。
- [ ] Sub to above：重复使用cookies而非每次都重新登录
- [ ] 进行分层决策（Utility-单个请求发送层级和Cookie Session管理；Method Priority-进行对Method的管理，对请求进行组织，如需要对多个点进行连续攻略等；Aim Priority-进行战略管理，分析达成目标需要的行为，直接根据InitGame中的信息进行决策，每个Task根据权重进行判断优先级和需要的Method）。
- [ ] Sub to above：使用response来update initgame中获取的用户信息
- [ ] InPlanning：库的裁剪（删掉一部分不需要用到的jar包）
- [ ] InPlanning：将输出内容的可读性提升
- [ ] InPlanning：找一个组织更好、拓展性更好的管理用户信息的方法
- [ ] InPlanning：处理由于网络中断带来的任务延迟
- [ ] InPlanning：加入自动修船功能
- [ ] InPlanning：一定概率忘收远征，看上去更像正常用户
- [ ] InPlanning：一定概率更换设备的UID号，看上去像是多设备登陆的正常用户

Task Schedule Design(4:00-24;00)

#### Method Priority

线程1：[上锁－从Schedule Queue中读取Method－下锁－执行任务－wait()]:LOOP

	Level 0. 由于演习、出击、击沉、肝船任务引发的前置（修理、等待远征、等待远征时指派的临时短时远征任务、等待资源）

#### Aim priority

线程2：[上锁－读取任务－向线程1类中的Schedule Queue加入Method－下锁－notify()线程1－wait()]:LOOP

	Level 1. 演习

	Level 2. 出击、击沉任务

	Level 3. 肝船

	Level 4. 建造、开发任务（设定公式）

	NOT-IN-SCHEDULE:Level 5. 强化任务（设计主要被强化的对象、设定稀有范围、拒绝等级超过1的船被当作狗粮、带new的船只到手上锁、被锁船不用于强化、婚舰不用于强化）

#### Time Slice

线程3？ScheduledPool－每16分钟唤醒AimPriority线程



问题：垃圾装备哪些需要废弃、哪些装备不需要？需不需要换装备？

流程：

判断是否有演习，向Level 0任务列表中填写演习前置任务。

流程：

（等待需要的船在进行的远征结束。等待修理补给用的资源。）