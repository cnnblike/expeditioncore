package com.cnnblike;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.Date;
import java.util.Iterator;

/**
 * Created by cnnblike_Mac on 15/8/16.
 */
public class Task implements Runnable{

    HttpClientTest Utility ;
    UserStatus userStatus;
    public Task(Setting value){
        Utility = new HttpClientTest(value);
        userStatus= new UserStatus();
    }
    public void NOP(){
        NOP(2000);
    }
    public void NOP(int x){
        try {
            Thread.sleep(x);
        }catch(Exception e){
            System.out.println(e.toString());
        }
    }

    public void updateLoginServer(String versionCheck){
        JSONObject js=JSON.parseObject(versionCheck);
        Utility.setting.setLoginServer(js.getString("loginServer"));
    }
    public String updateUserStatus(String initGameResult) {
        System.out.println("getUserStatus()");
        //System.out.println(initGameResult);
        JSONObject js = JSONObject.parseObject(initGameResult);
        userStatus.setSystime(js.getLong("systime"));
        System.out.println(js.getLong("systime"));
        userStatus.pveExploreStatuslevel =JSON.parseArray(js.getJSONObject("pveExploreVo").getJSONArray("levels").toJSONString(),PveExploreStatuslevel.class);
        for(Iterator<PveExploreStatuslevel> pesit=userStatus.pveExploreStatuslevel.listIterator();pesit.hasNext();){
            PveExploreStatuslevel ss = pesit.next();
            System.out.println(ss.getExploreId()+":"+ss.getFleetId()+":"+ss.getEndTime()+":"+ss.getStartTime());
        }
        return "SYSTIME:" + (Long.toString(userStatus.getSystime()));
    }
    public void resentAll(){
        for(Iterator<PveExploreStatuslevel> pesit=userStatus.pveExploreStatuslevel.listIterator();pesit.hasNext();){
            PveExploreStatuslevel ss = pesit.next();
            if(ss.getEndTime()<=userStatus.getSystime()) {
                String ExpResult=Utility.getPveExploreResult(ss.getExploreId());
                NOP();
                String sentResult=Utility.sentPveExplore(ss.getFleetId(),ss.getExploreId());
                NOP();
            }
        }
    }
    @SuppressWarnings("deprecation")
    public void run() {
        String VersionCheck=Utility.checkVersion();
        //System.out.println(VersionCheck);
        NOP();
        String initConfig=Utility.getInitConfig();NOP();
        //System.out.println(initConfig);
        if (Utility.setting.isAutomaticallyGetServer()){
            updateLoginServer(VersionCheck);
        }
        String passportLogin=Utility.passportLogin();
        NOP();
        //System.out.println(passportLogin);
        Utility.updateUserId(passportLogin);
        String loginResult=Utility.login();
        NOP();
//        System.out.println(loginResult);
        String initGameResult=Utility.initGame();NOP();
        updateUserStatus(initGameResult);NOP();
        //System.out.println(initGameResult);
        String UserDataResult=Utility.getUserData();
        resentAll();
        System.out.println("-----------------------------"+new Date().toLocaleString());
    }

}