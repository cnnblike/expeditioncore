package com.cnnblike;

/**
 * Created by cnnblike_Mac on 15/8/17.
 */
public class PveExploreStatuslevel{
    private String exploreId;
    private String fleetId;
    private long startTime;
    private long endTime;
    public PveExploreStatuslevel(){
        exploreId="";
        fleetId="";
        startTime=0L;
        endTime=0L;
    }
    public String getExploreId(){return exploreId;}
    public String getFleetId(){return fleetId;}
    public long getStartTime(){return startTime;}
    public long getEndTime(){return endTime;}
    public void setExploreId(String value){exploreId=value;}
    public void setFleetId(String value){fleetId=value;}
    public void setStartTime(long value){startTime=value;}
    public void setEndTime(long value){endTime=value;}
};