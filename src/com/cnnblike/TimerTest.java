package com.cnnblike;

import com.alibaba.fastjson.JSON;
import java.io.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by cnnblike_Mac on 15/8/16.
 */
public class TimerTest {
    public static void main(String[] args) {
        String path="./config.json";
        if (args.length!=0)
            path=args[0];
        String str="";
        try
        {
            BufferedReader br=new BufferedReader(new FileReader(path));
            String r=br.readLine();
            while(r!=null){
                str=str+r;
                r=br.readLine();
            }
        } catch (Exception e){
            System.out.println("Something Wrong");
            System.out.println(e.getStackTrace());
            System.out.println(e.toString());
            return ;
        }
        Setting setting = JSON.parseObject(str,Setting.class);
        System.out.println(setting.getVersion().toString());
        Task t1 = new Task(setting);
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(1);
        pool.scheduleWithFixedDelay(t1,0,16, TimeUnit.MINUTES);
    }
}