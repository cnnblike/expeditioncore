package com.cnnblike;

/**
 * Created by cnnblike_Mac on 15/8/17.
 */
// A Java Bean class
public class Setting implements java.io.Serializable{
    private String VersionServer;
    private String LoginServer;
    private String GameServer;
    private boolean AutomaticallyGetServer;
    private String Market;
    private String Channel;
    private String Version;
    private String SecretKey;
    private boolean UseZip;
    private String Username;
    private String Password;
    private String TalkingDataId;
    private String UniqueDeviceId;
    public Setting(){
        VersionServer="";
        LoginServer="";
        GameServer="";
        AutomaticallyGetServer=false;
        Market="";
        Channel="";
        Version="";
        SecretKey="";
        UseZip=false;
        Username="";
        Password="";
        TalkingDataId="";
        UniqueDeviceId="";
    }
    public String getVersionServer(){return VersionServer;}
    public String getLoginServer(){return LoginServer;}
    public String getGameServer(){return GameServer;}
    public boolean isAutomaticallyGetServer(){return AutomaticallyGetServer;}
    public String getMarket(){return Market;}
    public String getChannel(){return Channel;}
    public String getVersion(){return Version;}
    public String getSecretKey(){return SecretKey;}
    public boolean isUseZip(){return UseZip;}
    public String getUsername(){return Username;}
    public String getPassword(){return Password;}
    public String getTalkingDataId(){return TalkingDataId;}
    public String getUniqueDeviceId(){return UniqueDeviceId;}
    public void setVersionServer(final String value){VersionServer=value;}
    public void setLoginServer(final String value){LoginServer=value;}
    public void setGameServer(final String value){GameServer=value;}
    public void setAutomaticallyGetServer(final boolean value){AutomaticallyGetServer=value;}
    public void setMarket(final String value){Market=value;}
    public void setChannel(final String value){Channel=value;}
    public void setVersion(final String value){Version=value;}
    public void setSecretKey(final String value){SecretKey=value;}
    public void setUseZip(final boolean value){UseZip=value;}
    public void setUsername(final String value){Username=value;}
    public void setPassword(final String value){Password=value;}
    public void setTalkingDataId(final String value){TalkingDataId=value;}
    public void setUniqueDeviceId(final String value){UniqueDeviceId=value;}
    public void setUserId(final String UserId){}
}
