package com.cnnblike;

import java.util.Date;
import java.util.TimerTask;

/**
 * Created by cnnblike_Mac on 15/8/16.
*/
public class Task2 extends TimerTask {

    @SuppressWarnings("deprecation")
    @Override
    public void run() {
        System.out.println("----task2 start--------"+new Date().toLocaleString());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("----5s later, task2 end--------"+new Date().toLocaleString());
    }
}