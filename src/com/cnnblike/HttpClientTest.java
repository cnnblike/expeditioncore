package com.cnnblike;

import org.apache.http.*;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONArray;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/**
 * Created by cnnblike_Mac on 15/8/13.
 */
public class HttpClientTest {

    public Setting setting;

    private String mCookies = "";
    HashMap<String,String> mCookieStore=new HashMap<String,String>();
    Random random = new Random();
    private String mTalkingDataId = "3da9927b89041846242656a8b1231291b";
    private String mUserId;

    public HttpClientTest(Setting value){
        setting=value;
    }
    public String updateTimeStamp() {
        long milli = System.currentTimeMillis() + 8 * 3600 * 1000;
        long ticks = (milli * 10000) + 621355968000000000L;
        ticks = ticks + random.nextInt(1000) * 10;
        return Long.toString(ticks);
    }
    public String encryptValue(String source) {
        char  hexDigits[] = {  '0' ,  '1' ,  '2' ,  '3' ,  '4' ,  '5' ,  '6' ,  '7' ,  '8' ,  '9' ,
                'a' ,  'b' ,  'c' ,  'd' ,  'e' ,  'f'  };
        try  {
            byte [] strTemp = source.getBytes();
            MessageDigest mdTemp = MessageDigest.getInstance("MD5" );
            mdTemp.update(strTemp);
            byte [] md = mdTemp.digest();
            int  j = md.length;
            char  str[] =  new   char [j *  2 ];
            int  k =  0 ;
            for  ( int  i =  0 ; i < j; i++) {
                byte  byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4  &  0xf ];
                str[k++] = hexDigits[byte0 & 0xf ];
            }
            return   new  String(str);
        } catch  (Exception e) {
            return   null ;
        }
    }
    private static byte[] decompress(byte[] zipByte) throws IOException {
        ByteArrayOutputStream aos = new ByteArrayOutputStream();
        Inflater inflater = new Inflater();
        inflater.setInput(zipByte);
        byte[] buff = new byte[4096];
        int byteNum = 0;
        try {
            while (!inflater.finished()) {
                byteNum = inflater.inflate(buff, 0, 4096);
                aos.write(buff, 0, byteNum);
            }
        } catch (DataFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return aos.toByteArray();
    }
    private void updateCookie(String value) {
        String name=value.substring(0, value.indexOf("="));
        mCookieStore.put(name, value);
        Iterator itr= mCookieStore.values().iterator();
        mCookies="";
        while(itr.hasNext()){
            String StoredValue=itr.next().toString();
            mCookies=mCookies+StoredValue+";";
        }
        mCookies=mCookies.substring(0,mCookies.length()-1);
    }

    HttpRequestRetryHandler myRetryHandler= new HttpRequestRetryHandler() {
        public boolean retryRequest(IOException exception,int executionCount,HttpContext context){
            if (executionCount>=5) return false;
            if (exception instanceof NoHttpResponseException) return true;
            if (exception instanceof ConnectTimeoutException) return true;
            if (exception instanceof InterruptedIOException) return true;
            return false;
        }
    };
    CloseableHttpClient chc = HttpClients.custom().setRetryHandler(myRetryHandler).build();


    private String POSTMethod(String urlString, String server, String PARA_STRING, boolean AddCookie) {
        String timeString = "&t=" + updateTimeStamp();
        String encryptString = "&e=" + encryptValue(urlString + timeString + setting.getSecretKey());
        String extraString = "&gz=";
        if (setting.isUseZip()) extraString = extraString + "1";
        else extraString = extraString + "0";
        extraString = extraString + "&market=" + setting.getMarket() + "&channel=" + setting.getChannel() + "&version=" + setting.getVersion();
        HttpPost httpPost = new HttpPost(server + urlString + timeString + encryptString + extraString);
        //System.out.println(httpPost.toString());
        httpPost.addHeader("X-Unity-Version", "4.6.4f1");
        httpPost.addHeader("charset", "UTF-8");
        httpPost.addHeader("User-Agent", "Dalvik/1.6.0 (Linux; U; Android 4.1.1; Custom Phone - 4.1.1 - API 16 - 768x1280 Build/JRO03S)");
        httpPost.addHeader("Connection", "Keep-Alive");
        StringEntity entity = null;
        try {
            entity = new StringEntity(PARA_STRING);
            httpPost.setEntity(entity);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        if (setting.isUseZip()) httpPost.addHeader("Accept-Encoding", "gzip");
        if ((!mCookies.equals("")) && AddCookie) httpPost.addHeader("Cookie", mCookies);
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
        String resultString = "";
        try {
            CloseableHttpResponse response = chc.execute(httpPost);
            Header[] RH = response.getAllHeaders();
            for (Header hd : RH)
                if (hd.getName().toLowerCase().equals("set-cookie"))
                    updateCookie(hd.getValue());
            HttpEntity httpEntity = response.getEntity();
            if (setting.isUseZip()) {
            byte[] CompressedResult = EntityUtils.toByteArray(httpEntity);
            byte[] result = decompress(CompressedResult);
            resultString = new String(result);
            } else {
                resultString=EntityUtils.toString(httpEntity);
            }
        } catch (Exception e) {
            System.out.println("Something Wrong:" + e.toString());
        }
        return resultString;
    }
    private String GETMethod(String urlString, String server, boolean AddCookie) {
        String timeString = "&t=" + updateTimeStamp();
        String encryptString = "&e=" + encryptValue(urlString + timeString + setting.getSecretKey());
        String extraString = "&gz=";
        if (setting.isUseZip()) extraString = extraString + "1";
        else extraString = extraString + "0";
        extraString = extraString + "&market=" + setting.getMarket() + "&channel=" + setting.getChannel() + "&version=" + setting.getVersion();
        HttpGet httpGet = new HttpGet(server + urlString + timeString + encryptString + extraString);
        //System.out.println(httpGet.toString());
        httpGet.addHeader("X-Unity-Version", "4.6.4f1");
        httpGet.addHeader("charset", "UTF-8");
        httpGet.addHeader("User-Agent", "Dalvik/1.6.0 (Linux; U; Android 4.1.1; Custom Phone - 4.1.1 - API 16 - 768x1280 Build/JRO03S)");
        httpGet.addHeader("Connection", "Keep-Alive");
        if (setting.isUseZip()) httpGet.addHeader("Accept-Encoding", "gzip");
        if ((!mCookies.equals("")) && AddCookie) httpGet.addHeader("Cookie", mCookies);
        String resultString = "";
        try {
            CloseableHttpResponse response = chc.execute(httpGet);
            Header[] RH = response.getAllHeaders();
            for (Header hd : RH)
                if (hd.getName().toLowerCase().equals("set-cookie"))
                    updateCookie(hd.getValue());
            HttpEntity httpEntity = response.getEntity();
            if (setting.isUseZip()) {
                byte[] CompressedResult = EntityUtils.toByteArray(httpEntity);
                byte[] result = decompress(CompressedResult);
                resultString = new String(result);
            } else {
                resultString=EntityUtils.toString(httpEntity);
            }
        } catch (Exception e) {
            System.out.println("Something Wrong:" + e.toString());
        }
        return resultString;
    }

    public String checkVersion() {
        System.out.println("CheckVersion()");
        String urlString = "index/checkVer/" + setting.getMarket() + "/" + setting.getChannel();
        return GETMethod(urlString, setting.getVersionServer(), false);
    }

    public String getInitConfig() {
        System.out.println("getInitConfig()");
        String urlString = "index/getInitConfigs/";//not typo here.
        String PARA_STRING = "{}";
        if (setting.getLoginServer().equals("http://login.alpha.p7game.com/"))
            return POSTMethod(urlString, setting.getLoginServer(), PARA_STRING, false);//guofu
        else
            return GETMethod(urlString, setting.getLoginServer(), true);
    }
    public String passportLogin() {
        System.out.println("passportLogin()");
        String urlString = "index/passportLogin/" + setting.getUsername() + "/" + setting.getPassword();
        String PARA_STRING = "{\"deviceId\":\"" +"null" + "\",\"uid\":\"" + setting.getUsername() + "\",\"pwd\":\"" + setting.getPassword() + "\"}";
        return POSTMethod(urlString, setting.getLoginServer(), PARA_STRING, true);
    }
    public String login() {
        System.out.println("login()");
        String urlString = "index/login/" + mUserId;
        String PARA_STRING = "{\"deviceId\":\"\"}";
        return POSTMethod(urlString, setting.getGameServer(), PARA_STRING, true);
    }
    public boolean updateUserId(String passportLoginResult) {
        JSONObject js = JSONObject.parseObject(passportLoginResult);
        mUserId = js.getString("userId");
        System.out.println("UserID Update:" + mUserId);
        return true;
    }

    public String initGame() {
        System.out.println("initGame():");
        //updateUniqueDeviceId();
        String urlString = "api/initGame/" + setting.getUniqueDeviceId() + "/";//not typo here
        return GETMethod(urlString, setting.getGameServer(), true);
    }

    public String getUserData() {
        System.out.println("getUserData()");
        String urlString = "active/getUserData";
        return GETMethod(urlString, setting.getGameServer(), true);
    }
    public String getPveExploreResult(String explorePoint) {
        System.out.println("getPveExploreResult:" + explorePoint);
        String urlString = "explore/getResult/" + explorePoint;
        return GETMethod(urlString, setting.getGameServer(), true);
    }
    public String sentPveExplore(String fleetId,String explorePoint) {
        System.out.println("sentPveExplore:"+fleetId+"+"+explorePoint);
        String urlString = "explore/start/" + fleetId + "/" + explorePoint;
        String PARA_STRING = "{\"deviceId\":\"\"}";
        return POSTMethod(urlString, setting.getGameServer(), PARA_STRING, true);
    }

}
